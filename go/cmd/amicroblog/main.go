package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go"
	auth "firebase.google.com/go/auth"
	"github.com/pkg/errors"

	ae "github.com/k1350/amicroblog/internal/errors"
	"github.com/k1350/amicroblog/internal/repository"
)

var (
	client *firestore.Client
	ac     *auth.Client
	jst    *time.Location
)

type Page struct {
	Content []map[string]interface{} `json:"content"`
	HasNext bool                     `json:"has_next"`
	NextId  string                   `json:"next_id"`
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	err := root(w, r)
	if err != nil {
		http.Error(w, fmt.Sprintf("...: %w", err), 500)
		log.Fatalf("%+v", err)
	}
}

func root(w http.ResponseWriter, r *http.Request) error {
	next := r.URL.Query().Get("next")

	documents, err := repository.Get(client, next)
	if err != nil {
		return err
	}

	var posts []map[string]interface{}
	for _, doc := range documents.Docs {
		item := doc.Data()
		v := make(map[string]interface{})
		v["id"] = doc.Ref.ID
		v["text"] = item["text"]
		v["created"] = item["created"].(time.Time).In(jst)
		v["updated"] = item["updated"].(time.Time).In(jst)
		posts = append(posts, v)
	}

	p := Page{
		Content: posts,
		HasNext: documents.HasNext,
		NextId:  documents.LastId,
	}
	var buf bytes.Buffer
	enc := json.NewEncoder(&buf)
	if err = enc.Encode(&p); err != nil {
		return errors.WithStack(errors.WithMessage(ae.ParseError, err.Error()))
	}
	_, err = fmt.Fprint(w, buf.String())
	if err != nil {
		return errors.WithStack(errors.WithMessage(ae.IOError, err.Error()))
	}
	return nil
}

func inputHaldler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		h := r.Header["Authorization"]
		idToken := strings.Split(h[0], " ")

		ctx := context.Background()
		_, err := ac.VerifyIDToken(ctx, idToken[1])
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		err = input(w, r)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("%+v", err)
			return
		}
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func input(w http.ResponseWriter, r *http.Request) error {
	err := r.ParseMultipartForm(1024 * 5)
	if err != nil {
		return errors.WithStack(errors.WithMessage(ae.ParseError, err.Error()))
	}
	return repository.Add(r.MultipartForm.Value["text"][0], client)
}

func main() {
	var err error
	time.Local = time.FixedZone("Local", 9*60*60)
	jst, err = time.LoadLocation("Local")
	if err != nil {
		panic(err)
	}
	ctx := context.Background()
	app, err := firebase.NewApp(ctx, nil)
	if err != nil {
		panic(err)
	}
	client, err = app.Firestore(ctx)
	if err != nil {
		panic(err)
	}
	ac, err = app.Auth(ctx)
	if err != nil {
		panic(err)
	}
	defer client.Close()

	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/input", inputHaldler)
	http.ListenAndServe(":8000", nil)
}
