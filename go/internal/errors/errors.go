package errors

import (
	"github.com/pkg/errors"
)

var (
	ParseError     = errors.New("Parse Error")
	IOError        = errors.New("IO Error")
	FirestoreError = errors.New("Firestore IO Error")
)
