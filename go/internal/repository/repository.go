package repository

import (
	"context"
	"time"

	"cloud.google.com/go/firestore"
	"github.com/pkg/errors"

	ae "github.com/k1350/amicroblog/internal/errors"
)

const limit = 20

type Docs struct {
	Docs    []*firestore.DocumentSnapshot
	HasNext bool
	LastId  string
}

type Content struct {
	Text    string    `firestore:"text,omitempty"`
	Created time.Time `firestore:"created,omitempty"`
	Updated time.Time `firestore:"updated,omitempty"`
}

func Get(client *firestore.Client, next string) (Docs, error) {
	ctx := context.Background()
	pc := client.Collection("posts")

	var iter *firestore.DocumentIterator
	if next != "" {
		dsnap, err := pc.Doc(next).Get(ctx)
		if err != nil {
			return Docs{}, errors.WithStack(errors.WithMessage(ae.FirestoreError, err.Error()))
		}
		iter = pc.OrderBy("created", firestore.Desc).StartAt(dsnap.Data()["created"]).Limit(limit + 1).Documents(ctx)
	} else {
		iter = pc.OrderBy("created", firestore.Desc).Limit(limit + 1).Documents(ctx)
	}

	docs, err := iter.GetAll()
	if err != nil {
		return Docs{}, errors.WithStack(errors.WithMessage(ae.FirestoreError, err.Error()))
	}
	hasNext := len(docs) > limit

	lastId := ""
	if hasNext {
		lastDoc := docs[len(docs)-1]
		lastDocRef := lastDoc.Ref
		lastId = lastDocRef.ID
		docs = docs[0 : len(docs)-1]
	}

	return Docs{Docs: docs, HasNext: hasNext, LastId: lastId}, nil
}

func Add(text string, client *firestore.Client) error {
	ctx := context.Background()

	c := Content{
		Text:    text,
		Created: time.Now().UTC(),
		Updated: time.Now().UTC(),
	}
	_, _, err := client.Collection("posts").Add(ctx, c)
	if err != nil {
		return errors.WithStack(errors.WithMessage(ae.FirestoreError, err.Error()))
	}
	return nil
}
