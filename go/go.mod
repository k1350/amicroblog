module github.com/k1350/amicroblog

go 1.16

require (
	cloud.google.com/go/firestore v1.6.0
	firebase.google.com/go v3.13.0+incompatible
	github.com/pkg/errors v0.9.1
)
