module.exports = {
  purge: ['./src/*.{js,jsx,ts,tsx}', './src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {},
    fontFamily: {
      'sans': ['Arial', 'Helvetica Neue', 'Helvetica', '"Yu Gothic Medium"', 'YuGothic', '"Hiragino Kaku Gothic ProN"', 'Meiryo', 'sans-serif'],
    },
    container: {
      center: true,
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
