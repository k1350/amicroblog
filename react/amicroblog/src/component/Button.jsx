import React from 'react';

function Button(props) {
  return <button className="bg-transparent dark:hover:bg-blue-900 dark:hover:bg-opacity-10 text-gray-500 hover:text-blue-500 py-2 px-4 border border-gray-500 hover:border-blue-500 rounded" onClick={() => props.onClick()}>{props.name}</button>;
}

export default Button;