import React from 'react';

function InputText(props) {
  return (
    <div className="grid grid-cols-1 gap-y-px">
      <label className="text-gray-500" for={props.id}>{props.label}</label>
      <input className="dark:bg-gray-700 border border-gray-500 rounded p-0.5 text-gray-900 dark:text-white" type={props.type} id={props.id} name={props.name} onChange={props.onChange} value={props.value} />
    </div>
  );
}

export default InputText;