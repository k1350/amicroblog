import { useState } from "react";
import { app } from '../../config/firebase';
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

export const useSignin = () => {
    const [loginId, setLoginId] = useState("");
    const [password, setPassword] = useState("");
    const [errorMessage, setErrorMessage] = useState("");
    const auth = getAuth();

    const handleChange = (event) => {
        switch (event.target.name) {
            case 'loginId':
                setLoginId(event.target.value);
                break;
            case 'password':
                setPassword(event.target.value);
                break;
            default:
                console.log('key not found');
                break;
        }
    };

    const handleSubmit = (event) => {
        event.preventDefault();

        if (loginId === '' || password === '') {
            return;
        }

        signInWithEmailAndPassword(auth, loginId, password)
            .then((userCredential) => {
                const user = userCredential.user;
                console.log(user);
                window.location.href = "/input";
            })
            .catch((error) => {
                setErrorMessage("認証できませんでした。");
            });
    };

    return [loginId, password, errorMessage, { handleChange, handleSubmit }];
};