import { useState, useEffect } from 'react';

export const useList = () => {
    const [items, setItems] = useState([]);
    const [nextId, setNextId] = useState(null);
    const [hasNext, setHasNext] = useState(false);
    const [loadNext, setLoadNext] = useState(true);

    useEffect(() => {
        async function getItems() {
            let url = process.env.REACT_APP_API_BASE_URL;
            if (hasNext) {
                url += "?next=" + nextId;
            }
            fetch(url)
                .then(response => response.json())
                .then(data => {
                    setItems(items.concat(data.content));
                    setNextId(data.next_id);
                    setHasNext(data.has_next);
                });
        }
        if (loadNext) {
            getItems();
            setLoadNext(false);
        }
    }, [items, nextId, hasNext, loadNext]);

    return [items, hasNext, { setLoadNext }];
};