import { useState } from "react";
import { app } from '../../config/firebase';
import { getAuth, onAuthStateChanged } from "firebase/auth";

export const useText = () => {
    const [text, setText] = useState("");

    const handleChange = (event) => {
        switch (event.target.name) {
            case 'text':
                setText(event.target.value);
                break;
            default:
                console.log('key not found');
        }
    };

    const XHR = new XMLHttpRequest();
    XHR.addEventListener("load", function (event) {
        setText('');
    });

    XHR.addEventListener("error", function (event) {
        alert('エラーが発生しました');
    });

    const handleSubmit = (event) => {
        event.preventDefault();

        if (text === '') {
            return;
        }

        const auth = getAuth();
        var unsubscribe = onAuthStateChanged(auth, (user) => {
            if (user) {
                user.getIdToken(true).then(function (idToken) {
                    const FD = new FormData(document.getElementById('input-form'));
                    XHR.open("POST", process.env.REACT_APP_API_BASE_URL + '/input');
                    XHR.setRequestHeader('Authorization', 'Bearer ' + idToken);
                    XHR.send(FD);
                }).catch(function (error) {
                    alert(error);
                });
            }
        })
        unsubscribe();
    };

    return [text, { handleChange, handleSubmit }];
};