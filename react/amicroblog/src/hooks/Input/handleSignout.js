import { app } from '../../config/firebase';
import { getAuth, signOut } from "firebase/auth";

export default function handleSignout() {
    const auth = getAuth();
    signOut(auth).then(() => {
        window.location.href = "/signin";
    }).catch((error) => {
        console.log(error.message);
    });
};