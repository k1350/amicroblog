import { useState, useEffect } from "react";
import { app } from '../../config/firebase';
import { getAuth, onAuthStateChanged } from "firebase/auth";

export const useAuth = () => {
    const [signinCheck, setSigninCheck] = useState({ signinCheck: false, signedIn: false });

    useEffect(() => {
        const auth = getAuth();
        var unsubscribe = onAuthStateChanged(auth, (user) => {
            if (user) {
                setSigninCheck({ signinCheck: true, signedIn: true });
            } else {
                setSigninCheck({ signinCheck: true, signedIn: false });
            }
        })
        unsubscribe();
    }, []);

    return [signinCheck];
};