import React from 'react';
import { Redirect } from 'react-router-dom';
import { useAuth } from "./hooks/Auth/useAuth";

function NotAuth(props) {
    const [signinCheck] = useAuth();
    if (!signinCheck.signinCheck) {
        return (
            <div>Loading...</div>
        );
    }
    if (signinCheck.signedIn) {
        return <Redirect to="/" />
    } else {
        return props.children;
    }
}

export default NotAuth;