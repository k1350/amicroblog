import React from "react";
import { useSignin } from './hooks/Signin/useSignin';
import InputText from "./component/InputText";
import Submit from "./component/Submit";

function Form() {
    const [loginId, password, errorMessage, { handleChange, handleSubmit }] = useSignin();

    return (
        <main>
            <form className="my-2 grid grid-cols-1 gap-y-2" id="signin-form" onSubmit={handleSubmit}>
                <InputText type="text" id="loginId" name="loginId" onChange={handleChange} value={loginId} label="ログインID" />
                <InputText type="password" id="password" name="password" onChange={handleChange} value={password} label="パスワード" />
                <Submit value="ログイン" />
                <div className="text-gray-900 dark:text-white">{errorMessage}</div>
            </form>
        </main>
    );
}

function Signin() {
    return (
        <div className="container max-w-screen-sm">
            <Form />
        </div>
    );
}

export default Signin;
