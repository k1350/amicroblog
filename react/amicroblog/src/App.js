import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import List from './List';
import Signin from './Signin';
import NotAuth from './NotAuth';
import Error from "./Error";

export default function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={List} />
        <NotAuth>
          <Switch>
            <Route exact path="/signin" component={Signin} />
            <Route component={Error} />
          </Switch>
        </NotAuth>
      </Switch>
    </Router>
  );
}