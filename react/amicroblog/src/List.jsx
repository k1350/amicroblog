import React from 'react';
import dayjs from 'dayjs';
import { useList } from './hooks/List/useList';
import Button from './component/Button';
import Input from './Input';

function Blog(props) {
  const items = props.items;
  return (
    <ol className="grid grid-cols-1 gap-2 divide-y">
      {items.map((item) => {
        return (
          <li key={item.id}>
            <div className="text-gray-900 dark:text-white whitespace-pre-wrap break-words mb-2">{item.text}</div>
            <div className="text-sm text-gray-500">
              <time dateTime={item.updated}>
                {dayjs(item.updated).format('YYYY-MM-DD HH:mm:ss')}
              </time>
            </div>
          </li>
        );
      })}
    </ol>
  );
}

function NextButton(props) {
  const hasNext = props.hasNext;
  if (hasNext) {
    return <Button onClick={() => props.onClick()} name="次へ" />
  }
  return null;
}

function Main() {
  const [items, hasNext, { setLoadNext }] = useList();

  return (
    <main className="mx-8 my-10 grid grid-cols-1 gap-4">
      <Input />
      <Blog items={items} />
      <NextButton hasNext={hasNext} onClick={() => setLoadNext(true)} />
    </main>
  );
}

function List() {
  return (
    <div className="container max-w-screen-sm">
      <Main />
    </div>
  );
}

export default List;
