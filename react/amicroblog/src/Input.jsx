import React from "react";
import { useText } from './hooks/Input/useText';
import { useAuth } from "./hooks/Auth/useAuth";
import handleSignout from "./hooks/Input/handleSignout";
import Button from './component/Button';
import Submit from "./component/Submit";

function Form() {
  const [text, { handleChange, handleSubmit }] = useText();

  return (
    <form className="my-2 grid grid-cols-1 gap-y-2" id="input-form" onSubmit={handleSubmit}>
      <textarea className="dark:bg-gray-700 border border-gray-500 rounded p-0.5 text-gray-900 dark:text-white" id='text' name='text' value={text} onChange={handleChange} rows="5" cols="100" />
      <Submit value="送信" />
    </form>
  );
}

function Signout() {
  return (
    <Button onClick={handleSignout} name="ログアウト" />
  );
}

function Input() {
  const [signinCheck] = useAuth();

  if (signinCheck.signinCheck && signinCheck.signedIn) {
    return (
      <div>
        <Form />
        <Signout />
      </div>
    );
  }
  return null;
}

export default Input;
