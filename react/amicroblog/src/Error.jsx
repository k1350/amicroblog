import React from 'react';

const Error = () => {

    return (
        <div className="text-gray-900 dark:text-white">
            <p>お探しのページは存在しません。</p>
        </div>
    )
}
export default Error;